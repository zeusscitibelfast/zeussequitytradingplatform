package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.BollingerDao;
import com.citi.training.trader.model.BollingerBand;

@Component
public class BollingerService {

	 @Autowired
	    private BollingerDao bollingerDao;
	 

	 public List<BollingerBand> getStocks() {
	        return bollingerDao.getStocks();
	    }
}
