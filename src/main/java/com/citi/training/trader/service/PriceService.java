package com.citi.training.trader.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

@Component
public class PriceService {

    @Autowired
    private PriceDao priceDao;

    public int create(Price price) {
        return priceDao.create(price);
    }

    public List<Price> findAll(Stock stock) {
        return priceDao.findAll(stock);
    }

    public List<Price> findLatest(Stock stock, int count) {
        return priceDao.findLatest(stock,  count);
    }

    public int deleteOlderThan(Date cutOffTime) {
        return priceDao.deleteOlderThan(cutOffTime);
    }
    public List<Price> findLatestAllStocks(){
    	return priceDao.findLatestAllStocks();
    }
}
