package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.SimpleStrategyDao;
import com.citi.training.trader.model.SimpleStrategy;

@Component
public class SimpleStrategyService {

	@Autowired
	private StockService stockService;

    @Autowired
    private SimpleStrategyDao simpleStrategyDao;

    public List<SimpleStrategy> findAll(){
        return simpleStrategyDao.findAll();
    }

    public int save(SimpleStrategy strategy) {
    	if(strategy.getStock().getId() <= 0) {
    		strategy.setStock(stockService.findByTicker(strategy.getStock().getTicker().toLowerCase()));
    	}
        return simpleStrategyDao.save(strategy);
    }
}
