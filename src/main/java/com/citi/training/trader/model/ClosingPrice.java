package com.citi.training.trader.model;

public class ClosingPrice {
	
	private int stockId;
	private String ticker;
	private String date;
	private double closePrice;
	
	public ClosingPrice(int stockId, String ticker, String date, double closePrice) {
		this.stockId = stockId;
		this.ticker = ticker;
		this.date = date;
		this.closePrice = closePrice;
	}
	
	public int getStockId() {
		return stockId;
	}
	public void setStockId(int stockId) {
		this.stockId = stockId;
	}
	public String getTicker() {
		return ticker;
	}
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public double getClosePrice() {
		return closePrice;
	}
	public void setClosePrice(double closePrice) {
		this.closePrice = closePrice;
	}
	
	
	

}
