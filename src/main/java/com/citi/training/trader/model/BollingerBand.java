package com.citi.training.trader.model;


public class BollingerBand {

	private int stockId;
	private String ticker;
	private String date;
	private double lowerBand;
	private double middleBand;
	private double upperBand;

	public BollingerBand(int stockId, String ticker, 
			String date, double lowerBand, 
			double middleBand,double upperBand) {

		this.stockId = stockId;
		this.ticker = ticker;
		this.date = date;
		this.lowerBand = lowerBand;
		this.middleBand = middleBand;
		this.upperBand = upperBand;
	}

	public BollingerBand(int stockId, String ticker, double lowerBand, 
			double middleBand,double upperBand) {

		this.stockId = stockId;
		this.ticker = ticker;
		this.lowerBand = lowerBand;
		this.middleBand = middleBand;
		this.upperBand = upperBand;
	}

	public int getStockId() {
		return stockId;
	}

	public void setStockId(int stockId) {
		this.stockId = stockId;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public double getLowerBand() {
		return lowerBand;
	}

	public void setLowerBand(double lowerBand) {
		this.lowerBand = lowerBand;
	}

	public double getMiddleBand() {
		return middleBand;
	}

	public void setMiddleBand(double middleBand) {
		this.middleBand = middleBand;
	}

	public double getUpperBand() {
		return upperBand;
	}

	public void setUpperBand(double upperBand) {
		this.upperBand = upperBand;
	}

	 
	 
	 


}
