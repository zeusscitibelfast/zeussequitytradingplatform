package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.BollingerBand;

public interface BollingerDao {
 	
 	List<BollingerBand> getStocks();

}
