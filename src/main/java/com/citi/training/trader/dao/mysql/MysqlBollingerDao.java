package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.lang.Math;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.BollingerDao;
import com.citi.training.trader.model.BollingerBand;
import com.citi.training.trader.model.Stock;

@Component
public class MysqlBollingerDao implements BollingerDao {

	private static final Logger logger = LoggerFactory.getLogger(MysqlBollingerDao.class);

	@Autowired
	private JdbcTemplate tpl;

	@Autowired
	NamedParameterJdbcTemplate bollingerParameterJdbcTemplate;

	private static String GET_STOCK_TICKERS = "SELECT id, ticker FROM stock";

	// max bollinger days is 20
	private static String GET_CLOSING_PRICES = "SELECT stockId, ticker, closingPrice " + " FROM closing_prices"
			+ " JOIN stock ON closing_prices.stockId = stock.id "
			+ " WHERE closeDate >= (NOW()- INTERVAL 1 DAY)- INTERVAL 20 DAY AND ticker = '";

	private static String PUSH_BOLLINGER_BANDS = "INSERT INTO bollinger_bands "
			+ "(stockId, dateRecorded, lowerBand, middleBand, upperBand) "
			+ "VALUES (:stockId, :dateRecorded, :lowerBand, :middleBand, " + ":upperBand)";
//	private static String UPDATE_BOLLINGER_BANDS = "UPDATE bollinger_bands SET lowerBand=:lowerBand, " 
//											+ "middleBand=:middleBand, upperBand=:upperBand "
//											+ "WHERE stockId=:stockId AND dateRecorded=:dateRecorded";
	private static String GET_BOLLINGR_BANDS = "SELECT stockId, ticker, dateRecorded, lowerBand, middleBand, upperBand\r\n" + 
												"FROM bollinger_bands "
												+ "JOIN stock on stockId = stock.id " + 
												"WHERE dateRecorded = ";	

	@Override
	public List<BollingerBand> getStocks() {
		List<BollingerBand> bands = getBollingerBands();
		logger.info("Number of bands: " + bands.size());
		if(bands.size()==0) {
			logger.info("Bands Not Calculated. Running Calculation");
			List<Stock> stocks = this.tpl.query(GET_STOCK_TICKERS, new StockMapper());
			return getClosingPrices(stocks);
		}else {
			return bands;
		}
	}

	
	public List<BollingerBand> getClosingPrices(List<Stock> stocks) {
		HashMap<Stock, List<Double>> closingPrices = new HashMap<>();
		for (int i = 0; i < +stocks.size(); i++) {
			String queryString = GET_CLOSING_PRICES + stocks.get(i).getTicker() + "' LIMIT 20";
			closingPrices.put(stocks.get(i), this.tpl.query(queryString, new ClosingPriceMapper()));
		}
		return createBollingerBands(closingPrices);
	}

	// Use bollinger algorithm to create band limits
	public List<BollingerBand> createBollingerBands(HashMap<Stock, List<Double>> closingPrices) {
		logger.info("Looping Through Closing Prices for Stocks");
		List<BollingerBand> bollingerBands = new ArrayList<BollingerBand>();
		for (Map.Entry<Stock, List<Double>> stock : closingPrices.entrySet()) {
			Stock s = stock.getKey();
			List<Double> closes = stock.getValue();
			double totalCloseValues = 0;
			double simpleMovingAvg = 0;

			double deviationValue = 0;
			for (int i = 0; i < closes.size(); i++) {
				totalCloseValues += closes.get(i);
			}
			simpleMovingAvg = totalCloseValues / 20;
			for (int i = 0; i < closes.size(); i++) {
				deviationValue += Math.pow((closes.get(i) - simpleMovingAvg), 2);
			}
			deviationValue = Math.sqrt((deviationValue / 20));
			double upperBollingerBand = simpleMovingAvg + (2 * deviationValue);
			double lowerBollingerBand = simpleMovingAvg - (2 * deviationValue);

			bollingerBands.add(new BollingerBand(s.getId(), s.getTicker(), lowerBollingerBand, simpleMovingAvg,
					upperBollingerBand));
		}
		bollingerBands.sort(Comparator.comparing(BollingerBand::getStockId));
		pushBandsToDatabase(bollingerBands);
		
		return getBollingerBands();
	}

	public void pushBandsToDatabase(List<BollingerBand> bollingerBands) {

		logger.info("Bands Not calculated for "+ LocalDate.now()+"-  Pushing to DB" );
		for (int i = 0; i < bollingerBands.size(); i++) {
			logger.debug("Adding Bollinger Values for stockId: " + bollingerBands.get(i).getStockId() 
					+ " To Database for  "+ LocalDate.now());
			MapSqlParameterSource bandParameters = new MapSqlParameterSource();
			bandParameters.addValue("stockId", bollingerBands.get(i).getStockId());
			bandParameters.addValue("dateRecorded", LocalDate.now());
			bandParameters.addValue("lowerBand", bollingerBands.get(i).getLowerBand());
			bandParameters.addValue("middleBand", bollingerBands.get(i).getMiddleBand());
			bandParameters.addValue("upperBand", bollingerBands.get(i).getUpperBand());
			bollingerParameterJdbcTemplate.update(PUSH_BOLLINGER_BANDS, bandParameters);
		}
	}
	
	public List<BollingerBand> getBollingerBands(){
		logger.info("Bands Found in Database For " + LocalDate.now() );
		
		List<BollingerBand> allBands = this.tpl.query(GET_BOLLINGR_BANDS + "'" + LocalDate.now()+"'",  
											new BollingerBandMapper());

		return allBands;
	}


	private static final class ClosingPriceMapper implements RowMapper<Double> {
		public Double mapRow(ResultSet rs, int rowNum) throws SQLException {
			logger.debug("Mapping Closing prices for  [" + rs.getString("ticker") + " ]");
			return rs.getDouble("closingPrice");
		}
	}

	private static final class StockMapper implements RowMapper<Stock> {
		public Stock mapRow(ResultSet rs, int rowNum) throws SQLException {
			logger.debug("getting stock ticker [ " + rs.getString("ticker") + " ]");

			return new Stock(rs.getInt("id"), rs.getString("ticker"));
		}
	}
	private static final class BollingerBandMapper implements RowMapper<BollingerBand> {
		public BollingerBand mapRow(ResultSet rs, int rowNum) throws SQLException {
			logger.debug("getting Bollinger Band For  [ " + rs.getString("ticker") + " ]");
			BollingerBand b = new BollingerBand(rs.getInt("stockId"), 
									rs.getString("ticker"), 
									rs.getDouble("lowerBand"), 
									rs.getDouble("middleBand"),
									rs.getDouble("upperBand"));

			return b;
		}
	}
}

