package com.citi.training.trader.rest;

import java.util.List; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.service.SimpleStrategyService;

@CrossOrigin(origins = "${com.citi.training.trader.cross-origin-host:http://localhost:4200}")
@RestController
@RequestMapping("${com.citi.training.trader.rest.trade-base-path:/strategy}")
public class StrategyController {

	@Autowired
	SimpleStrategyService simpleStrategyService;

	private static final Logger logger =
			LoggerFactory.getLogger(StrategyController.class);

	@RequestMapping(method=RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<SimpleStrategy> create(@RequestBody SimpleStrategy simpleStrategy) {
		logger.debug("create(" + simpleStrategy + ")");

		simpleStrategy.setId(simpleStrategyService.save(simpleStrategy));
		logger.debug("created Strategy: " + simpleStrategy);

		return new ResponseEntity<SimpleStrategy>(simpleStrategy, HttpStatus.CREATED);
	}

	@RequestMapping(method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public List<SimpleStrategy> findAll() {
		logger.debug("findAll()");
		return simpleStrategyService.findAll();
	}


}
