package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.BollingerBand;
import com.citi.training.trader.service.BollingerService;

@CrossOrigin(origins = "${com.citi.training.trader.cross-origin-host:http://localhost:4200}")
@RestController
@RequestMapping("${com.citi.training.trader.rest.trade-base-path:/engine}")
public class BollingerController {

	private static final Logger logger =
			LoggerFactory.getLogger(BollingerController.class);
	@Autowired
	private BollingerService bollingerService;

	@RequestMapping(method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public List<BollingerBand> getStocks() {
		logger.debug("getBollingerBands for all stocks");
		return bollingerService.getStocks();
	}
}
